from django.core.mail import EmailMessage


class Util:

    @staticmethod
    def send_email(data):
        email_body = f'Hi {data["user"]}, Use link below to verify {data["abs_url"]}'

        email_subject = "Verify"

        email = EmailMessage(
            subject=email_subject, body=email_body, to=[data["target_email"]]
        )
